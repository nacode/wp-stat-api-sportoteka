<?php
/*
Plugin Name: Sportoteka API Integration
Description: Plugin for integrating with Sportoteka API on post creation.
Version: 1.0
Author: wpFace
Author URI: https://wpface.ru/
*/





include 'functions.php';


function get_current_player_id()
{

    //return 20422;
    $post_id = get_the_ID();

    $playerId = get_field('player-id-sportoteka');




    if (is_null($playerId) || strlen($playerId) == 0) {
        return 34950;
    } else {
        return $playerId;
    }
}

function get_post_field_acf($field)
{

    $tagValue = get_field($field);


    if (is_null($tagValue) || strlen($tagValue) == 0) {
        return 34950;
    } else {
        return $tagValue;
    }
}


// Регистрация шорткода
add_shortcode('show_player_career_sportoteka', 'show_player_id_career_sportoteka_shortcode');

// Функция обработки шорткода
function show_player_id_career_sportoteka_shortcode()
{

    $tags = array("msl", "vtbyouth", "vtb", "vtbleague", "wsl", "dubl-b", "dubl-g", "rfb-deti", "uba");

    $result = array();

    foreach ($tags as $key => $value) {
        $rs = careerFetch($value);
        $result = array_merge($result, $rs->items);
    }


    //print_r($result);

    // print_r($result);

    renderPlayerCareer($result);
}

function careerFetch($tag)
{


    $RequestVerificationToken = "CfDJ8F1IZ1Gvha1Csg1hjDcC3bumKthfcKNsq13qhC3Yo-dLGJaad_snEs7aqqRt6fM6OHylQFNc2PSATd0dXdLtcggJECHB6Z6ZVtDqpiFOKwPdHJApggY_3OrPrrbmMYa9A8guw8KBDFkkf5Fw7C4MeKo";

    $playerId = null;

    if (isset($atts['playerid'])) {
        $playerId = $atts['playerid'];
    } else {
        $playerId = get_current_player_id();
    }


    $url = "https://basket.sportoteka.org/api/" . "abc/players/career?id=" . $playerId . "&Tag=" . $tag;

    $headers = array(
        'accept: text/plain',
        'RequestVerificationToken:' . $RequestVerificationToken,
        'X-Requested-With: XMLHttpRequest'
    );



    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);



    // Execute cURL request
    $response = curl_exec($ch);

    
    // Close cURL session
    

    

    // Decode the JSON response
    $rs = json_decode($response);

    

    return $rs;
}


// Hook to execute the function on post creation
// Регистрация шорткода
add_shortcode('show_player_stat_sportoteka', 'show_player_id_stat_sportoteka_shortcode');


// Функция обработки шорткода
function show_player_id_stat_sportoteka_shortcode($atts)
{



    $RequestVerificationToken = "CfDJ8F1IZ1Gvha1Csg1hjDcC3bumKthfcKNsq13qhC3Yo-dLGJaad_snEs7aqqRt6fM6OHylQFNc2PSATd0dXdLtcggJECHB6Z6ZVtDqpiFOKwPdHJApggY_3OrPrrbmMYa9A8guw8KBDFkkf5Fw7C4MeKo";

    $playerId = null;

    if (isset($atts['playerid'])) {
        $playerId = $atts['playerid'];
    } else {
        $playerId = get_current_player_id();
    }


    $url = "https://basket.sportoteka.org/api/" . "abc/players/stats?id=" . $playerId;

    $headers = array(
        'accept: text/plain',
        'RequestVerificationToken:' . $RequestVerificationToken,
        'X-Requested-With: XMLHttpRequest'
    );



    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);



    // Execute cURL request
    $response = curl_exec($ch);


    // Close cURL session
    curl_close($ch);

    // Decode the JSON response
    $rs = json_decode($response);

    renderPlayerStats($rs);
}




add_shortcode('show_player_info_sportoteka', 'show_player_id_info_sportoteka_shortcode');

// Функция обработки шорткода
function show_player_id_info_sportoteka_shortcode($atts)
{


    $RequestVerificationToken = "CfDJ8F1IZ1Gvha1Csg1hjDcC3bumKthfcKNsq13qhC3Yo-dLGJaad_snEs7aqqRt6fM6OHylQFNc2PSATd0dXdLtcggJECHB6Z6ZVtDqpiFOKwPdHJApggY_3OrPrrbmMYa9A8guw8KBDFkkf5Fw7C4MeKo";

    $playerId = null;

    if (isset($atts['playerid'])) {
        $playerId = $atts['playerid'];
    } else {
        $playerId = get_current_player_id();
    }

    $url = "https://basket.sportoteka.org/api/" . "abc/players/info?id=" . $playerId;

    $headers = array(
        'accept: text/plain',
        'RequestVerificationToken:' . $RequestVerificationToken,
        'X-Requested-With: XMLHttpRequest'
    );

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);



    // Execute cURL request
    $response = curl_exec($ch);


    // Close cURL session
    curl_close($ch);

    // Decode the JSON response
    $rs = json_decode($response);

    renderPlayerInfo($rs->result);
}
