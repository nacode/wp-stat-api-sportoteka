<?php
	


	function renderPlayerInfo($data){

		if(is_admin()){
			return '';
		}

		ob_start();

		$seasons = array_reverse($data->seasons);
		$teams = array_reverse($data->teams);

		?>
		<style>
		<?= require('./wp-content/plugins/wp-stat-api-sportoteka/style.css') ?>
		</style>
		<div class="item-info">
				<span class="label">Фамилия:</span>
				<span class="value"><?= $data->lastName ?></span>
			</div>
			<div class="item-info">
				<span class="label">Имя:</span>
				<span class="value"><?= $data->firstName ?></span>
			</div>
			<div class="item-info">
				<span class="label">Дата рождения:</span>
				<span class="value"><?= $data->birthday ?></span>
			</div>
			<div class="item-info">
				<span class="label">Возраст:</span>
				<span class="value"><?= $data->age ?></span>
			</div>
			<div class="item-info">
				<span class="label">Пол:</span>
				<span class="value"><?= $data->gender == 'Male' ? 'Мужчина' : 'Женщина' ?></span>
			</div>
			<div class="item-info">
				<span class="label">Страна:</span>
				<span class="value"><?= $data->country->name ?></span>
			</div>
			<div class="item-info">
				<img width="150px" src="<?= $data->photo ?>">
			</div>
			<table class="info-table" border="1">
			  <tr>
			    <th>Сезон</th>			    
			    <th>Команда</th>			    
			  </tr>
			  <?php foreach ($seasons as $key => $value) {			  	
			  	?>
			  	<tr>
				    <td><?= $value->name ?></td>				    
				    <td><?= $teams[$key]->name ?> - <?= $teams[$key]->regionName ?></td>				    
			  	</tr>		
			  	<?php
			  } ?>
			  
			  
			</table>
		<?php

		$out = ob_get_clean();

		if($_SERVER['REQUEST_METHOD'] == "GET") echo $out;

		
	}

	function renderPlayerStats($data){

		
		ob_start();

		?>
			<style>
			<?= require_once('./wp-content/plugins/wp-stat-api-sportoteka/style.css'); ?>
			</style>
			<table class="info-table" border="1">
			  <tr>
			    <th>Дата</th>
			    <th>Команда</th>
			    <th>Очки</th>	
				<th>Счёт</th>
				
				<th>2П</th>			
				<th>2Б</th>		
				<th>2%</th>		

				<th>3П</th>							
				<th>3Б</th>			
				<th>ШП</th>			
				<th>ШБ</th>		
				
				<th>ИП</th>
				<th>ИБ</th>		
				
				<th>3%</th>		
				<th>И%</th>	
				
				
				<th>Ш%</th>		
				<th>РП</th>		
				<th>АП</th>	


				<th>ПХ</th>		
				<th>БШ</th>		
				<th>ПЗ</th>	
				<th>ПН</th>	


				<th>ПВ</th>		
				<th>ФС</th>	
				<th>ПТ</th>	


				<th>Ф</th>		
				<th>МИН</th>	
				
				
			  </tr>
			  <?php 
			  	$res = array_reverse($data->items);
			  ?>
			  <?php foreach ($res as $key => $value) {
			  	if(is_null($value->team)) continue;
			  	?>
			  	<tr>
				  	<td><?= $value->game->gameDate ?? ''?></td>
					<td><?= $value->game->team1Name ?? ''?> - <?= $value->game->team2Name ?? '' ?></td>				    
				    <td><?= $value->stats->points?></td>
					
					<td><?= $value->game->score?></td>

					<td><?= $value->stats->goal2 ?? '' ?></td>	
					<td><?= $value->stats->shot2 ?? '' ?></td>	
					<td><?= $value->stats->goal3 ?? '' ?></td>	

					<td><?= $value->stats->shot3 ?? '' ?></td>	
					<td><?= $value->stats->goal1 ?? '' ?></td>	
					<td><?= $value->stats->shot1 ?? '' ?></td>		
					
					<td><?= $value->stats->goal23 ?? '' ?></td>		
					<td><?= $value->stats->shot23 ?? '' ?></td>		
					<td><?= $value->stats->shot2Percent ?? '' ?></td>		
					<td><?= $value->stats->shot3Percent ?? '' ?></td>		
					<td><?= $value->stats->shot23Percent ?? '' ?></td>		

					<td><?= $value->stats->shot1Percent ?? '' ?></td>
					<td><?= $value->stats->assist ?? '' ?></td>
					<td><?= $value->stats->pass ?? '' ?></td>


					<td><?= $value->stats->steal ?? '' ?></td>
					<td><?= $value->stats->blockShot ?? '' ?></td>
					<td><?= $value->stats->defRebound ?? '' ?></td>
					<td><?= $value->stats->offRebound ?? '' ?></td>


					<td><?= $value->stats->rebound ?? '' ?></td>
					<td><?= $value->stats->foulsOnPlayer ?? '' ?></td>
					<td><?= $value->stats->turnover ?? '' ?></td>


					<td><?= $value->stats->foul ?? '' ?></td>
					<td><?= $value->stats->playedTime ?? '' ?></td>
					
					
			  	</tr>		
			  	<?php
			  } ?>
			  
			  
			</table>
		<?php

		$out = ob_get_clean();

		if($_SERVER['REQUEST_METHOD'] == "GET") echo $out;
	}




	function renderPlayerCareer($data){


		
		
		ob_start();

		?>
			<style>
			<?= require_once('./wp-content/plugins/wp-stat-api-sportoteka/style.css'); ?>
			</style>
			<table class="info-table" border="1">
			  <tr>
			    <th>Сезон</th>
			    <th>Команда</th>
			    <th>Очки</th>	
				
				<th>Игр</th>			
				<th>2П</th>			
				<th>2Б</th>					
				<th>3П</th>			
				
				<th>3Б</th>			
				<th>ШП</th>			
				<th>ШБ</th>		
				
				<th>ИП</th>
				<th>ИБ</th>		
				<th>2%</th>		
				<th>3%</th>		
				<th>И%</th>	
				
				
				<th>Ш%</th>		
				<th>РП</th>		
				<th>АП</th>	


				<th>ПХ</th>		
				<th>БШ</th>		
				<th>ПЗ</th>	
				<th>ПН</th>	


				<th>ПВ</th>		
				<th>ФС</th>	
				<th>ПТ</th>	


				<th>Ф</th>		
				<th>МИН</th>	
				<th>+/-</th>	
				
			  </tr>
			  <?php foreach ($data as $key => $value) {
			  	if(is_null($value->team)) continue;
			  	?>
			  	<tr>
				  
				  	
				  	<td><?= $value->season->name ? str_replace("Сезон", "", $value->season->name) : ''?></td>
				    <td><?= $value->team->name ?? ''?></td>				    
				    <td><?= $value->stats->points?></td>	

					<td><?= $value->stats->games ?></td>

					<td><?= $value->stats->goal2 ?? '' ?></td>	
					<td><?= $value->stats->shot2 ?? '' ?></td>	
					<td><?= $value->stats->goal3 ?? '' ?></td>	

					<td><?= $value->stats->shot3 ?? '' ?></td>	
					<td><?= $value->stats->goal1 ?? '' ?></td>	
					<td><?= $value->stats->shot1 ?? '' ?></td>		
					
					<td><?= $value->stats->goal23 ?? '' ?></td>		
					<td><?= $value->stats->shot23 ?? '' ?></td>		
					<td><?= $value->stats->shot2Percent ?? '' ?></td>		
					<td><?= $value->stats->shot3Percent ?? '' ?></td>		
					<td><?= $value->stats->shot23Percent ?? '' ?></td>		

					<td><?= $value->stats->shot1Percent ?? '' ?></td>
					<td><?= $value->stats->assist ?? '' ?></td>
					<td><?= $value->stats->pass ?? '' ?></td>


					<td><?= $value->stats->steal ?? '' ?></td>
					<td><?= $value->stats->blockShot ?? '' ?></td>
					<td><?= $value->stats->defRebound ?? '' ?></td>
					<td><?= $value->stats->offRebound ?? '' ?></td>


					<td><?= $value->stats->rebound ?? '' ?></td>
					<td><?= $value->stats->foulsOnPlayer ?? '' ?></td>
					<td><?= $value->stats->turnover ?? '' ?></td>


					<td><?= $value->stats->foul ?? '' ?></td>
					<td><?= $value->stats->playedTime ?? '' ?></td>
					<td><?= $value->stats->plusMinus ?? '' ?></td>
					
			  	</tr>		
			  	<?php
			  } ?>
			  
			  
			</table>
		<?php

		$out = ob_get_clean();

		if($_SERVER['REQUEST_METHOD'] == "GET") echo $out;
	}

 ?>